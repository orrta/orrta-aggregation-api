# orrta-aggregation-api

[![Python Version](https://img.shields.io/badge/python-3.6-green.svg)](https://img.shields.io/badge/python-3.6-green.svg)
[![pipeline status](https://gitlab.com/orrta/orrta-aggregation-api/badges/master/pipeline.svg)](https://gitlab.com/orrta/orrta-aggregation-api/commits/master)


## Requirements
  - Python 3.6
  - Docker

## Get help
  - ``` make help ```

## How to setup
- ``` git clone https://gitlab.com/orrta/orrta-aggregation-api.git ```
- ``` cd orrta-aggregation-api ```
- ``` make setup ```

## How to run
- ``` make run_local ```

## Running tests

#### All
- ``` make test ```

#### Unit
- ``` make test_unit ```

#### Acceptance
- ``` make test_acceptance ```

## Run elasticsearch
- ``` make run_elasticsearch ```

## Stop elasticsearch
- ``` make stop_elasticsearch ```

## Open kibana
- ``` make open_kibana ```

> User: elastic
> Password: changeme
