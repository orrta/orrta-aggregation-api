def get_bus_message_fixture(fixture_name):
    fixture_file_path = 'tests/fixtures/{fixture_name}.xml'.format(
        fixture_name=fixture_name)

    with open(fixture_file_path) as f:
        return f.read()
