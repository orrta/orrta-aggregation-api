import json

from unittest.mock import patch, ANY

from orrta_aggregation_api.web.main import application

from tornado.testing import AsyncHTTPTestCase

from tests.unit import mocked_es_connection


class TestIndex(AsyncHTTPTestCase):

    def setUp(self):
        super().setUp()

        self.test_document = {'title': 'New document'}

    def get_app(self):
        return application

    @patch('orrta_aggregation_api.web.elasticsearch.ElasticsearchHandler.es_connection')
    def test_index_new_document(self, es_connection):
        es_response = {
            "_index": "index",
            "_type": "default",
            "_id": "AVUH6MyvoHPo6hFWyrTr",
            "_version": 1,
            "_shards": {
                "total": 2,
                "successful": 1,
                "failed": 0
                },
            "created": True
        }

        es_connection.index.return_value = mocked_es_connection(201, es_response)

        response = self.fetch(
            '/index',
            method='POST',
            headers={
                'Content-type': 'application/json',
                'Authorization': 'FADD90F58717711252A52E444278ECE5C24DD040F902C60280FDC1DD398746A8'
            },
            body=json.dumps(self.test_document)
        )
        self.assertEqual(response.code, 201)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {'es': es_response})

        es_connection.index.assert_called_with(
            index='index',
            doc_id=None,
            document={'title': 'New document'}
        )

    @patch('orrta_aggregation_api.web.elasticsearch.ElasticsearchHandler.es_connection')
    def test_index_new_document_with_specific_id(self, es_connection):
        es_response = {
            '_index': 'index',
            '_type': 'default',
            '_id': 'document_id',
            '_version': 1,
            '_shards': {
                'total': 2,
                'successful': 1,
                'failed': 0
            },
            'created': True
        }

        es_connection.index.return_value = mocked_es_connection(201, es_response)

        response = self.fetch(
            '/index/document_id',
            method='POST',
            headers={
                'Content-type': 'application/json',
                'Authorization': 'FADD90F58717711252A52E444278ECE5C24DD040F902C60280FDC1DD398746A8'
            },
            body=json.dumps(self.test_document))

        self.assertEqual(response.code, 201)
        self.assertDictEqual(
            json.loads(response.body.decode('utf-8')), {'es': es_response})

        es_connection.index.assert_called_with(
            index='index',
            doc_id='document_id',
            document={'title': 'New document'}
        )


class TestUpdate(AsyncHTTPTestCase):

    def setUp(self):
        super().setUp()

        self.test_document = {
            'title': 'Update document'
        }

    def get_app(self):
        return application

    @patch('orrta_aggregation_api.web.elasticsearch.UpdateHandler.es_connection')
    def test_update_document(self, es_connection):
        es_response = {
            '_index': 'index',
            '_type': 'default',
            '_id': 'id',
            '_version': 2,
            '_shards': {
                'total': 2,
                'successful': 1,
                'failed': 0
            }
        }

        es_connection.update.return_value = mocked_es_connection(200, es_response)

        response = self.fetch(
            '/index/document_id/update',
            method='POST',
            headers={
                'Content-type': 'application/json',
                'Authorization': 'FADD90F58717711252A52E444278ECE5C24DD040F902C60280FDC1DD398746A8'
            },
            body=json.dumps(self.test_document))

        self.assertEqual(response.code, 200)
        self.assertDictEqual(
            json.loads(response.body.decode('utf-8')), {'es': es_response})

        es_connection.update.assert_called_with(
            index='index',
            doc_id='document_id',
            document={'title': 'Update document'}
        )


class TestDelete(AsyncHTTPTestCase):
    def get_app(self):
        return application

    @patch('orrta_aggregation_api.web.elasticsearch.ElasticsearchHandler.es_connection')
    def test_delete_document(self, es_connection):
        es_response = {
            'found': True,
            '_index': 'index',
            '_type': 'default',
            '_id': 'id',
            '_version': 3,
            '_shards': {
                'total': 2,
                'successful': 1,
                'failed': 0
            }
        }

        es_connection.delete.return_value = mocked_es_connection(200, es_response)

        response = self.fetch(
            '/index/document_id',
            method='DELETE',
            headers={
                'Content-type': 'application/json',
                'Authorization': 'FADD90F58717711252A52E444278ECE5C24DD040F902C60280FDC1DD398746A8'
            })

        self.assertEqual(response.code, 200)
        self.assertDictEqual(
            json.loads(response.body.decode('utf-8')), {'es': es_response})

        es_connection.delete.assert_called_with(
            index='index',
            doc_id='document_id'
        )
