from os.path import join, dirname
from dotenv import load_dotenv

from tornado.concurrent import Future
from tornado.httpclient import HTTPResponse, HTTPRequest
from io import BytesIO

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


def mocked_async_return(return_value):
    result = Future()
    result.set_result(return_value)

    return result


def mocked_response(code, body, error=None):
    """Prepare a future response for HTTPrequests.

    Arguments:
        code: Status code
        body: Response content
        error: Exception object, if any
    """
    result = Future()
    result.set_result(mocked_sync_request(code, body, error))

    return result


def mocked_sync_request(code, body, error=None):
    """Prepare a future response for HTTPrequests.

    Arguments:
        code: Status code
        body: Response content
        error: Exception object, if any
    """
    request = HTTPRequest('elasticsearch.fake.com')
    buffer = BytesIO(str.encode(body))

    return HTTPResponse(request, code, buffer=buffer, error=error)


def mocked_es_wrapper(response_code=200, response_json={}):
    """Prepare a future response for Indexer call.

    Arguments:
        response_code: Status code
        es_response: Dict with es response
    """
    class MockedElasticsearchWrapperResponse:
        def __init__(self, response_code, response_json):
            self.success = 200 <= response_code <= 299
            self.status_code = response_code
            self.response_json = response_json

    result = MockedElasticsearchWrapperResponse(response_code, response_json)

    indexer = Future()
    indexer.set_result(result)

    return indexer


def mocked_es_connection(response_code=200, response_json={}, is_valid=True, validation_errors=[]):
    """Prepare a future response for Indexer call.

    Arguments:
        response_code: Status code
        es_response: Dict with es response
    """
    class MockedElasticsearchWrapperResponse:
        def __init__(self, response_code, response_json):
            self.success = 200 <= response_code <= 299
            self.status_code = response_code
            self.response_json = response_json

    class MockedElasticSearchResponse:
        def __init__(self, es_wrapper_response, is_valid=True, validation_errors=[]):
            self.es_wrapper_response = es_wrapper_response
            self.is_valid = is_valid
            self.validation_errors = validation_errors

    result = MockedElasticSearchResponse(
        es_wrapper_response=MockedElasticsearchWrapperResponse(
            response_code,
            response_json
        ),
        is_valid=is_valid,
        validation_errors=validation_errors
    )

    indexer = Future()
    indexer.set_result(result)

    return indexer
