import json

import tornado.httpclient
import tornado.testing

from unittest.mock import patch, Mock, call, ANY

from orrta_aggregation_api import settings
from orrta_aggregation_api.elasticsearch.es_wrapper import ElasticsearchWrapper

from tests.unit import mocked_response, mocked_es_wrapper, mocked_sync_request


@patch('orrta_aggregation_api.elasticsearch.es_wrapper.http_client')
class TestEsIndexer(tornado.testing.AsyncTestCase):
    def setUp(self):
        super().setUp()
        self.test_id = '1'
        self.test_index = 'index'
        self.test_type = 'type'
        self.test_document = {'title': 'Test Title'}
        self.response = {'sample_attr': 'sample_value'}
        self.headers = {'Content-Type': 'application/json'}

    @tornado.testing.gen_test
    def test_index_document(self, http_client):
        http_client.fetch.return_value = mocked_response(
            201, json.dumps(self.response))

        actual = yield ElasticsearchWrapper().index(
            index=self.test_index,
            doc_type=self.test_type,
            document=self.test_document
        )

        self.assertTrue(actual.success)
        self.assertEqual(actual.status_code, 201)
        self.assertEqual(actual.response_json, self.response)

        expected_url = '{0}/{1}/{2}'.format(
            settings.ELASTICSEARCH_URL, self.test_index, self.test_type)

        http_client.fetch.assert_called_with(
            expected_url,
            raise_error=False,
            auth_password='busca',
            auth_username='busca',
            body=json.dumps(self.test_document),
            headers=self.headers,
            method='POST'
        )

    @tornado.testing.gen_test
    def test_index_document_ensures_index_existence_when_index_exists(self, http_client):
        http_client.fetch.return_value = mocked_response(
            200, json.dumps(self.response))

        index = "index_test"

        yield ElasticsearchWrapper().index(
            index=index,
            doc_type=self.test_type,
            document=self.test_document
        )

        expected_url = '{0}/{1}'.format(
            settings.ELASTICSEARCH_URL, index)

        http_client.fetch.assert_has_calls([
            call(
                expected_url,
                auth_password='busca',
                auth_username='busca',
                method='HEAD'
            ),
            ANY
        ])

    @tornado.testing.gen_test
    def test_update_document(self, http_client):
        http_client.fetch.return_value = mocked_response(
            200, json.dumps(self.response))

        actual = yield ElasticsearchWrapper().update(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id,
            document=self.test_document
        )

        self.assertTrue(actual.success)
        self.assertEqual(actual.status_code, 200)
        self.assertEqual(actual.response_json, self.response)

        expected_url = '{0}/{1}/{2}/{3}/_update'.format(
            settings.ELASTICSEARCH_URL,
            self.test_index,
            self.test_type,
            self.test_id
        )
        http_client.fetch.assert_called_with(
            expected_url,
            raise_error=False,
            auth_password='busca',
            auth_username='busca',
            body=json.dumps(self.test_document),
            headers=self.headers,
            method='POST'
        )

    @tornado.testing.gen_test
    def test_delete_document(self, http_client):
        http_client.fetch.return_value = mocked_response(
            200, json.dumps(self.response))

        actual = yield ElasticsearchWrapper().delete(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id
        )

        self.assertTrue(actual.success)
        self.assertEqual(actual.status_code, 200)
        self.assertEqual(actual.response_json, self.response)

        expected_url = '{0}/{1}/{2}/{3}'.format(
            settings.ELASTICSEARCH_URL,
            self.test_index,
            self.test_type,
            self.test_id
        )

        http_client.fetch.assert_called_with(
            expected_url,
            raise_error=False,
            method='DELETE',
            auth_password=settings.ELASTICSEARCH_PASS,
            auth_username=settings.ELASTICSEARCH_USER
        )

    @tornado.testing.gen_test
    def test_delete_document_when_document_not_found(self, http_client):
        response_body = {
            'found': False,
            '_index': 'index_test',
            '_type': 'materia',
            '_id': 'AVkrSlyxNQEaW8WIlfM9',
            '_version': 2,
            '_shards': {
                'total': 2,
                'successful': 1,
                'failed': 0
            }
        }

        response = mocked_sync_request(
            code=404, body=json.dumps(response_body))

        http_client.fetch.side_effect = tornado.httpclient.HTTPError(
            code=404, response=response)

        actual = yield ElasticsearchWrapper().delete(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id
        )

        self.assertFalse(actual.success)
        self.assertEqual(actual.status_code, 404)
        self.assertEqual(actual.response_json, response_body)

    @tornado.testing.gen_test
    def test_remove_document_field(self, http_client):
        http_client.fetch.return_value = mocked_response(
            200, json.dumps(self.response))

        actual = yield ElasticsearchWrapper().remove_field(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id,
            field='field_test'
        )

        self.assertTrue(actual.success)
        self.assertEqual(actual.status_code, 200)
        self.assertEqual(actual.response_json, self.response)

        expected_url = '{0}/{1}/{2}/{3}/_update'.format(
            settings.ELASTICSEARCH_URL,
            self.test_index,
            self.test_type,
            self.test_id
        )
        expected_data = json.dumps({
            'script': 'ctx._source.remove("field_test")'
        })

        http_client.fetch.assert_called_with(
            expected_url,
            raise_error=False,
            auth_password='busca',
            auth_username='busca',
            body=expected_data,
            headers=self.headers,
            method='POST'
        )

    @tornado.testing.gen_test
    def test_index_or_update_when_document_exists(self, http_client):
        http_client.fetch.return_value = mocked_response(
            201, json.dumps({}))

        es_wrapper = ElasticsearchWrapper()
        es_wrapper.update = Mock(return_value=mocked_es_wrapper())

        yield es_wrapper.index_or_update(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id,
            document=self.test_document
        )

        es_wrapper.update.assert_called_with(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id,
            document=self.test_document
        )

    @tornado.testing.gen_test
    def test_index_or_update_when_document_does_not_exist(self, http_client):
        http_client.fetch.side_effect = tornado.httpclient.HTTPError(404)

        es_wrapper = ElasticsearchWrapper()
        es_wrapper.index = Mock(
            return_value=mocked_es_wrapper(201, {}))

        yield es_wrapper.index_or_update(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id,
            document=self.test_document
        )

        es_wrapper.index.assert_called_with(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id,
            document=self.test_document
        )
