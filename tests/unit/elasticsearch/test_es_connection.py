import tornado.httpclient
import tornado.testing

from unittest.mock import patch, Mock, ANY

from orrta_aggregation_api.elasticsearch.es_connection import ElasticSearchConnection

from tests.unit import mocked_es_wrapper, mocked_async_return


@patch('orrta_aggregation_api.elasticsearch.es_connection.es_wrapper')
class TestIndexerIndex(tornado.testing.AsyncTestCase):
    def setUp(self):
        super().setUp()
        self.test_id = '1'
        self.test_index = 'index'
        self.test_type = 'default'
        self.test_document = {'title': 'Test Title'}

    @tornado.testing.gen_test
    def test_index_should_index_when_document_valid(self, es_wrapper):
        es_wrapper.index = Mock(return_value=mocked_es_wrapper())

        es_connection = ElasticSearchConnection()
        yield es_connection.index(
            index=self.test_index,
            document=self.test_document,
            doc_id=self.test_id
        )

        es_wrapper.index.assert_called_with(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id,
            document=self.test_document
        )


@patch('orrta_aggregation_api.elasticsearch.es_connection.es_wrapper')
class TestIndexerUpdate(tornado.testing.AsyncTestCase):
    def setUp(self):
        super().setUp()
        self.test_id = '1'
        self.test_index = 'index'
        self.test_type = 'default'
        self.test_document = {'title': 'Test Title'}

    @tornado.testing.gen_test
    def test_update_should_update_document(self, es_wrapper):
        es_wrapper.update = Mock(return_value=mocked_es_wrapper())

        es_connection = ElasticSearchConnection()
        yield es_connection.update(
            index=self.test_index,
            document=self.test_document,
            doc_id=self.test_id
        )

        es_wrapper.update.assert_called_with(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id,
            document=self.test_document
        )


@patch('orrta_aggregation_api.elasticsearch.es_connection.es_wrapper')
class TestIndexerDelete(tornado.testing.AsyncTestCase):
    def setUp(self):
        super().setUp()
        self.test_id = '1'
        self.test_index = 'index'
        self.test_type = 'default'
        self.test_document = {'title': 'Test Title'}

    @tornado.testing.gen_test
    def test_delete_should_delete_document_when_document_exists(self, es_wrapper):
        es_wrapper.get_document = Mock(return_value=mocked_async_return({
            'found': True,
            '_index': self.test_index,
            '_id': self.test_id,
            '_source': self.test_document
        }))
        es_wrapper.delete = Mock(return_value=mocked_es_wrapper())

        es_connection = ElasticSearchConnection()
        yield es_connection.delete(
            index=self.test_index,
            doc_id=self.test_id
        )

        es_wrapper.delete.assert_called_with(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id
        )

    @tornado.testing.gen_test
    def test_delete_should_call_delete_document_when_document_dont_exist(self, es_wrapper):
        es_wrapper.get_document = Mock(return_value=mocked_async_return(False))
        es_wrapper.delete = Mock(return_value=mocked_es_wrapper(404, {}))

        es_connection = ElasticSearchConnection()
        yield es_connection.delete(
            index=self.test_index,
            doc_id=self.test_id
        )

        es_wrapper.delete.assert_called_with(
            index=self.test_index,
            doc_type=self.test_type,
            doc_id=self.test_id
        )
