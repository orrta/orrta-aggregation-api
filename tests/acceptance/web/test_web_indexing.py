import json
import time

from tests.acceptance.base_test_case import AcceptanceAsyncHTTPTestCase
from unittest.mock import ANY


class TestWebIndexing(AcceptanceAsyncHTTPTestCase):
    def test_should_index_new_document_with_http_request(self):
        # calls orrta_aggregation_api index service
        response = self.fetch(
            self.test_doc_api_path,
            method='POST',
            headers=self.valid_indexer_http_headers,
            body=json.dumps(self.test_valid_document)
        )

        self.assertEqual(response.code, 201)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                'es': {
                    '_version': 1,
                    '_index': self.test_doc_index,
                    '_type': self.test_doc_type,
                    '_id': self.test_doc_id,
                    '_shards': ANY,
                    'created': True,
                    'result': 'created'
                }
            }
        )

        self.assertElasticSearchDocument(
            self.test_doc_es_path,
            {
                'found': True,
                '_version': 1,
                '_index': self.test_doc_index,
                '_type': self.test_doc_type,
                '_id': self.test_doc_id,
                '_source': self.test_valid_document
            }
        )

    def test_should_create_aliased_index_when_indexing_with_http_request_and_index_does_not_exist(self):
        # calls orrta_aggregation_api index service
        response = self.fetch(
            self.test_unexistent_index_doc_api_path,
            method='POST',
            headers=self.valid_indexer_http_headers,
            body=json.dumps(self.test_valid_document)
        )

        self.assertEqual(response.code, 201)

        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                'es': {
                    '_version': 1,
                    '_index': self.test_doc_unexistent_real_index,
                    '_type': self.test_doc_type,
                    '_id': self.test_doc_id,
                    '_shards': ANY,
                    'created': True,
                    'result': 'created'
                }
            }
        )

        self.assertElasticSearchDocument(
            self.test_unexistent_index_doc_es_path,
            {
                'found': True,
                '_version': 1,
                '_index': self.test_doc_unexistent_real_index,
                '_type': self.test_doc_type,
                '_id': self.test_doc_id,
                '_source': self.test_valid_document
            }
        )

    def test_should_update_document_with_http_request(self):
        self.create_es_test_document()

        properties_to_update = {
            'new_property': 'property value'
        }

        # calls orrta_aggregation_api update service
        response = self.fetch(
            self.test_doc_api_path + '/update',
            method='POST',
            headers=self.valid_indexer_http_headers,
            body=json.dumps({'doc': properties_to_update})
        )

        self.assertEqual(response.code, 200)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                'es': {
                    '_version': 2,
                    '_index': self.test_doc_index,
                    '_type': self.test_doc_type,
                    '_id': self.test_doc_id,
                    '_shards': ANY,
                    'result': 'updated'
                }
            }
        )

        self.assertElasticSearchDocument(
            self.test_doc_es_path,
            {
                'found': True,
                '_version': 2,
                '_index': self.test_doc_index,
                '_type': self.test_doc_type,
                '_id': self.test_doc_id,
                '_source': {**self.test_valid_document, **properties_to_update}
            }
        )

    def test_should_respond_when_updating_non_existent_document(self):
        properties_to_update = {
            'new_property': 'property value'
        }

        # calls orrta_aggregation_api update service
        response = self.fetch(
            self.test_doc_api_path + '/update',
            method='POST',
            headers=self.valid_indexer_http_headers,
            body=json.dumps({'doc': properties_to_update})
        )

        self.assertEqual(response.code, 404)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                'es': {
                    'status': 404,
                    'error': {
                        'index': self.test_doc_index,
                        'type': 'document_missing_exception',
                        'reason': '[default][test-doc-id]: document missing',
                        "index_uuid": ANY,
                        'root_cause': ANY,
                        'shard': ANY
                    }
                }
            }
        )

    def test_should_delete_document_with_http_request(self):
        self.create_es_test_document()

        # calls orrta_aggregation_api delete service
        response = self.fetch(
            self.test_doc_api_path,
            method='DELETE',
            headers=self.valid_indexer_http_headers
        )

        self.assertEqual(response.code, 200)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                'es': {
                    'found': True,
                    '_version': 2,
                    '_index': self.test_doc_index,
                    '_type': self.test_doc_type,
                    '_id': self.test_doc_id,
                    '_shards': ANY,
                    'result': 'deleted'
                }
            }
        )

        self.assertElasticSearchDocument(
            self.test_doc_es_path,
            {
                'found': False,
                '_index': self.test_doc_index,
                '_type': self.test_doc_type,
                '_id': self.test_doc_id
            }
        )

    def test_should_respond_when_delete_non_existent_document(self):
        # calls orrta_aggregation_api delete service
        response = self.fetch(
            self.test_doc_api_path,
            method='DELETE',
            headers=self.valid_indexer_http_headers
        )

        self.assertEqual(response.code, 404)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                'es': {
                    'found': False,
                    '_version': 1,
                    '_index': self.test_doc_index,
                    '_type': self.test_doc_type,
                    '_id': self.test_doc_id,
                    '_shards': ANY,
                    'result': 'not_found'
                }
            }
        )

    def test_should_respond_when_get_existent_document(self):
        self.create_es_test_document()

        # calls orrta_aggregation_api get service
        response = self.fetch(
            self.test_doc_api_path,
            method='GET',
            headers=self.valid_indexer_http_headers
        )

        self.assertEqual(response.code, 200)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
              "es": {
                "_index": "index_acceptance-test",
                "_type": "default",
                "_id": "test-doc-id",
                "_version": 1,
                "found": True,
                "_source": {
                  "identifier": "id",
                  "title": "New document",
                  "body": "Document body",
                  "url": "http://acceptance-tests.com/1",
                  "issued": "2017-06-26T16:44:06.953Z",
                  "modified": "2017-06-26T16:15:12.645Z"
                }
              }
            }
        )

    def test_should_respond_when_get_non_existent_document(self):
        # calls orrta_aggregation_api get service
        response = self.fetch(
            self.test_unexistent_doc_api_path,
            method='GET',
            headers=self.valid_indexer_http_headers
        )

        self.assertEqual(response.code, 404)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                "es": {
                    "_index": "index_acceptance-test",
                    "_type": "default",
                    "_id": "unexistent",
                    "found": False
                }
            }
        )

    def test_should_respond_when_search_existent_document(self):
        self.create_es_test_document()

        # Wait indexing
        time.sleep(2)

        # calls orrta_aggregation_api search service
        response = self.fetch(
            '/' + self.test_doc_index + '/_search',
            method='POST',
            headers=self.valid_indexer_http_headers,
            body=json.dumps({
                "query": {
                    "match": {
                        "body": "New document"
                    }
                }
            })
        )

        self.assertEqual(response.code, 200)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                "es": {
                    "took": ANY,
                    "timed_out": ANY,
                    "_shards": ANY,
                    "hits": {
                        "total": 1,
                        "max_score": ANY,
                        "hits": [
                            {
                                "_index": "index_acceptance-test",
                                "_type": "default",
                                "_id": "test-doc-id",
                                "_score": ANY,
                                "_source": {
                                    "identifier": "id",
                                    "title": "New document",
                                    "body": "Document body",
                                    "url": "http://acceptance-tests.com/1",
                                    "issued": "2017-06-26T16:44:06.953Z",
                                    "modified": "2017-06-26T16:15:12.645Z"
                                }
                            }
                        ]
                    }
                }
            }
        )

    def test_should_respond_when_search_non_existent_document(self):
        self.create_es_test_document()

        # Wait indexing
        time.sleep(2)

        # calls orrta_aggregation_api search service
        response = self.fetch(
            '/' + self.test_doc_index + '/_search',
            method='POST',
            headers=self.valid_indexer_http_headers,
            body=json.dumps({
                "query": {
                    "match": {
                        "body": "text"
                    }
                }
            })
        )

        self.assertEqual(response.code, 200)
        self.assertDictEqual(
            json.loads(response.body.decode('UTF-8')), {
                "es": {
                    "took": ANY,
                    "timed_out": ANY,
                    "_shards": ANY,
                    "hits": {
                        "total": 0,
                        "max_score": ANY,
                        "hits": []
                    }
                }
            }
        )
