import json
import os
from urllib.parse import quote_plus, urljoin

from orrta_aggregation_api import settings
from orrta_aggregation_api.web.main import application
from tornado.ioloop import IOLoop
from tornado.testing import AsyncHTTPTestCase


class ElasticsearchAcceptanceTestCase():
    @property
    def test_doc_id(self):
        return 'test-doc-id'

    @property
    def test_doc_type(self):
        return 'default'

    @property
    def test_doc_index(self):
        return 'index_acceptance-test'

    @property
    def test_doc_unexistent_index(self):
        return 'unexistent_index'

    @property
    def test_doc_unexistent_real_index(self):
        return '.unexistent_index-v1'

    @property
    def test_doc_es_path(self):
        return '/{index}/{doc_type}/{doc_id}'.format(
            index=self.test_doc_index,
            doc_type=self.test_doc_type,
            doc_id=quote_plus(self.test_doc_id)
        )

    @property
    def test_unexistent_index_doc_es_path(self):
        return '/{index}/{doc_type}/{doc_id}'.format(
            index=self.test_doc_unexistent_index,
            doc_type=self.test_doc_type,
            doc_id=quote_plus(self.test_doc_id)
        )

    @property
    def test_doc_api_path(self):
        return '/{index}/{doc_id}'.format(
            index=self.test_doc_index,
            doc_id=quote_plus(self.test_doc_id)
        )

    @property
    def test_unexistent_doc_api_path(self):
        return '/{index}/{doc_id}'.format(
            index=self.test_doc_index,
            doc_id='unexistent'
        )

    @property
    def test_unexistent_index_doc_api_path(self):
        return '/{index}/{doc_id}'.format(
            index=self.test_doc_unexistent_index,
            doc_id=quote_plus(self.test_doc_id)
        )

    @property
    def valid_indexer_http_headers(self):
        return {
            'Content-type': 'application/json',
            'Authorization': 'FADD90F58717711252A52E444278ECE5C24DD040F902C60280FDC1DD398746A8'
        }

    @property
    def test_index_settings(self):
        return {
            'settings': {
                'index': {
                    'number_of_shards': 1,
                    'number_of_replicas': 0
                }
            }
        }

    @property
    def test_valid_document(self):
        return {
            'identifier': 'id',
            'title': 'New document',
            'body': 'Document body',
            'url': 'http://acceptance-tests.com/1',
            'issued': '2017-06-26T16:44:06.953Z',
            'modified': '2017-06-26T16:15:12.645Z'

        }

    def fetch_external(self, path, **kwargs):
        self.http_client.fetch(path, self.stop, **kwargs)
        return self.wait()

    def clean_test_index(self):
        self.es_request(self.test_doc_index, method='DELETE')
        self.es_request(self.test_doc_unexistent_index, method='DELETE')
        self.es_request(self.test_doc_unexistent_real_index, method='DELETE')

        '.{real_index_name}-v1'.format(
            real_index_name=self.test_doc_unexistent_index
        ),

        self.es_request(
            self.test_doc_index,
            method='PUT',
            json_body=self.test_index_settings
        )

    def es_request(self, path, method='GET', json_body=None):
        url = urljoin(settings.ELASTICSEARCH_URL, path)
        body = json.dumps(json_body) if json_body else None

        return self.fetch_external(
            url,
            method=method,
            body=body,
            auth_username=settings.ELASTICSEARCH_USER,
            auth_password=settings.ELASTICSEARCH_PASS
        )

    def create_es_test_document(self):
        self.es_request(
            self.test_doc_es_path, method='POST', json_body=self.test_valid_document)

    def assertElasticSearchDocument(self, path, expected_response_json):
        es_response = self.es_request(path)
        self.assertDictEqual(
            json.loads(es_response.body.decode('utf-8')),
            expected_response_json
        )

    def assertElasticSearchDocumentDoesNotExist(self, path):
        es_response = self.es_request(path)
        self.assertEqual(es_response.code, 404)


class AcceptanceAsyncHTTPTestCase(AsyncHTTPTestCase, ElasticsearchAcceptanceTestCase):
    def get_app(self):
        return application

    def get_new_ioloop(self):
        return IOLoop.instance()

    def setUp(self):
        super().setUp()
        self.clean_test_index()

    def tearDown(self):
        super().tearDown()
        self.clean_test_index()

    def fetch(self, path, **kwargs):
        test_host = os.environ.get('TEST_WEB_INDEXING_URL')

        if test_host:
            return self.fetch_external(test_host + path, **kwargs)

        return super().fetch(path, **kwargs)
