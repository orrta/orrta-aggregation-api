import os
from os.path import join, dirname
from dotenv import load_dotenv


test_env = os.environ.get('TEST_ENV', 'local')

env_file_path = 'env/{test_env}.env'.format(test_env=test_env)

dotenv_path = join(dirname(__file__), env_file_path)
load_dotenv(dotenv_path)
