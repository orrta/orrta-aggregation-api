import os
import sys
import logging

__author__ = 'orrtapp'
__email__ = 'orrtapp@gmail.com'
__version__ = '0.0.1'
__project__ = 'orrta_aggregation_api'

level = int(os.environ.get('ORRTA_API_LOG_LEVEL', 10))
logging.basicConfig(stream=sys.stdout, level=level)
