import os

settings_dir = os.path.join(os.path.dirname(__file__), 'settings')

MEASURES_URL = os.environ.get(
    'MEASURES_URL',
    'ec2-18-216-60-243.us-east-2.compute.amazonaws.com'
)

MEASURES_PORT = os.environ.get('MEASURES_PORT', 1984)

ELASTICSEARCH_USER = os.environ.get('ELASTICSEARCH_USER')
ELASTICSEARCH_PASS = os.environ.get('ELASTICSEARCH_PASS')
ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')

REDIS_URL = os.environ.get(
    'REDIS_URL',
    'redis://h:p5777486eba9f14fadf5a598c1dfa367f7bc1b51f59431977a2ae165ca81ea7e8@ec2-34-203-121-173.compute-1.amazonaws.com:7139'
)

AUTH_KEY = os.environ.get('AUTH_KEY', 'FADD90F58717711252A52E444278ECE5C24DD040F902C60280FDC1DD398746A8')

DEBUG = os.environ.get('DEBUG', False) == 'True'

ENVIROMENT = os.environ.get('ENVIROMENT', 'PROD')

CONTROLLED_ERROR = os.environ.get('CONTROLLED_ERROR', False) == 'True'

MOBILE_APP_SETTINGS = {
    'subscription': {
        'plans': [{
            'name': 'ASSINE POR R$9,90/MÊS',
            'id': 'jusilveira.subscription.monthly'
        }, {
            'name': 'ASSINE POR R$69,90/MÊS',
            'id': 'jusilveira.subscription.yearly'
        }],
        'background_image': 'https://res.cloudinary.com/marbamedia/image/upload/c_scale,w_800/v1528915058/Corpo/ACESS%C3%93RIOS%20DE%20PRAIA/Ju-54.jpg',
        'title': 'SEJA UM ASSINANTE',
        'subtitle': 'Acesse conteúdos exclusivos e tenha uma experiência única, feita especialmente para você.'
    }
}
