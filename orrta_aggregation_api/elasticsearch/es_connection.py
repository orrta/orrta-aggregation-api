from .es_wrapper import ElasticsearchWrapper

es_wrapper = ElasticsearchWrapper()


class ElasticSearchResponse:
    def __init__(self, es_wrapper_response=None, is_valid=True, validation_errors=[]):
        self.es_wrapper_response = es_wrapper_response
        self.is_valid = is_valid
        self.validation_errors = validation_errors


class ElasticSearchConnection:

    async def msearch(self, index, queries, cache_key=None):
        es_wrapper_response = await es_wrapper.msearch(
            index=index, doc_type='default', queries=queries, cache_key=cache_key
        )

        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)

    async def mget(self, index, targets, cache_key=None):
        if not targets or len(targets) == 0:
            return ElasticSearchResponse()

        es_wrapper_response = await es_wrapper.mget(
            index=index, doc_type='default', targets=targets, cache_key=cache_key
        )

        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)

    async def search(self, index, query, cache_key=None):
        es_wrapper_response = await es_wrapper.search(
            index=index, doc_type='default', query=query, cache_key=cache_key
        )

        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)

    async def index(self, index, document, doc_id=None):
        es_wrapper_response = await es_wrapper.index(
            index=index, doc_type='default', doc_id=doc_id, document=document
        )

        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)

    async def index_or_update(self, index, doc_id, document):
        es_wrapper_response = await es_wrapper.index_or_update(
            index=index, doc_type='default', doc_id=doc_id, document=document
        )

        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)

    async def update(self, index, doc_id, document):
        es_wrapper_response = await es_wrapper.update(
            index=index, doc_type='default', doc_id=doc_id, document=document
        )

        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)

    async def delete(self, index, doc_id):
        es_wrapper_response = await es_wrapper.delete(index=index, doc_type='default', doc_id=doc_id)
        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)

    async def get(self, index, doc_id):
        cache_key = '{index}:{id}'.format(
            index=index,
            id=doc_id
        )

        es_wrapper_response = await es_wrapper.get_document(
            index=index, doc_type='default', doc_id=doc_id, cache_key=cache_key
        )

        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)

    async def remove_field(self, index, doc_id, field):
        es_wrapper_response = await es_wrapper.remove_field(
            index=index, doc_type='default', doc_id=doc_id, field=field
        )

        return ElasticSearchResponse(es_wrapper_response=es_wrapper_response)
