from measures import Measure
from orrta_aggregation_api import settings
from tornado.httpclient import AsyncHTTPClient, HTTPError
from random import randint
import json
import logging
import redis
import urllib

http_client = AsyncHTTPClient()
logger = logging.getLogger('web')
redis_connection = redis.Redis.from_url(settings.REDIS_URL)

measure = Measure('es-wrapper', (settings.MEASURES_URL, settings.MEASURES_PORT))


def get_response_body(es_response):
    body = es_response.body

    try:
        body = es_response.body.decode('UTF-8')
    except AttributeError:
        pass

    return body


class DocumentNotFoundException(Exception):
    def __init__(self, document_id):
        message = 'Document not found for id {0}'.format(document_id)
        super().__init__(message)


class CacheResponse:
    def __init__(self, body):
        self.code = body['code']
        self.body = body['body']


class ElasticsearchWrapperResponse:
    def __init__(self, es_response):
        self.success = 200 <= es_response.code <= 299
        self.status_code = es_response.code
        self.response = get_response_body(es_response)
        self.response_json = json.loads(self.response)


class ElasticsearchWrapper:

    ONE_DAY = 86400

    CACHE_TTL_SECONDS_GET = ONE_DAY
    CACHE_TTL_SECONDS_SEARCH = ONE_DAY
    CACHE_TTL_SECONDS_MULTI = ONE_DAY

    def __init__(self):
        self.elasticsearch_url = settings.ELASTICSEARCH_URL
        self._headers = {'Content-Type': 'application/json'}

    async def get_document(self, index, doc_type, doc_id, cache_key=None):
        url = self.generate_es_url(index, doc_type, doc_id)

        dimensions = {
            'index': index,
            'doc_type': doc_type,
            'doc_id': doc_id,
            'cache_key': cache_key
        }

        try:
            measure.count('get', dimensions=dimensions)

            if cache_key:
                cache_value = self._get_cache(cache_key)

                if cache_value:
                    indexer_response = ElasticsearchWrapperResponse(cache_value)
                    return indexer_response

            response = await http_client.fetch(
                url,
                auth_username=settings.ELASTICSEARCH_USER,
                raise_error=False,
                auth_password=settings.ELASTICSEARCH_PASS,
            )

            indexer_response = ElasticsearchWrapperResponse(response)

            if cache_key:
                self._set_cache(cache_key, response, self.CACHE_TTL_SECONDS_GET)

            return indexer_response
        except HTTPError as http_error:
            logging.warning('ORRTA API Error getting {0}'.format(url))

            error_dimensions = {
                'search': dimensions,
                'status_code': http_error.code,
                'error': get_response_body(http_error.response)
            }

            measure.count('get-error', dimensions=error_dimensions)

            if http_error.code == 404:
                return ElasticsearchWrapperResponse(http_error.response)

            raise http_error

    async def search(self, index, doc_type, query, cache_key=None):
        url = self.generate_es_url(index, doc_type, action='_search')

        dimensions = {
            'index': index,
            'doc_type': doc_type,
            'cache_key': cache_key
        }

        measure.count('search', dimensions=dimensions)

        if cache_key:
            cache_value = self._get_cache(cache_key)

            if cache_value:
                indexer_response = ElasticsearchWrapperResponse(cache_value)
                return indexer_response

        response = await http_client.fetch(
            url,
            method='POST',
            raise_error=False,
            auth_username=settings.ELASTICSEARCH_USER,
            auth_password=settings.ELASTICSEARCH_PASS,
            headers=self._headers,
            body=json.dumps(query)
        )

        indexer_response = ElasticsearchWrapperResponse(response)

        if indexer_response.success:
            if cache_key:
                self._set_cache(cache_key, response, self.CACHE_TTL_SECONDS_SEARCH)
        else:
            logging.warning('ORRTA API Error searching - {0}'.format(url))

            error_dimensions = {
                'search': dimensions,
                'status_code': indexer_response.status_code,
                'error': get_response_body(indexer_response.response)
            }

            measure.count('search-error', dimensions=error_dimensions)

        return indexer_response

    async def mget(self, index, doc_type, targets, cache_key=None):
        url = self.generate_es_url(index, doc_type, action='_mget')

        dimensions = {
            'index': index,
            'doc_type': doc_type,
            'targets': targets,
            'cache_key': cache_key
        }

        measure.count('mget', dimensions=dimensions)

        headers = self._headers
        headers['Content-Type'] = 'application/json'

        if cache_key:
            cache_value = self._get_cache(cache_key)

            if cache_value:
                indexer_response = ElasticsearchWrapperResponse(cache_value)
                return indexer_response

        response = await http_client.fetch(
            url,
            method='POST',
            raise_error=False,
            auth_username=settings.ELASTICSEARCH_USER,
            auth_password=settings.ELASTICSEARCH_PASS,
            headers=headers,
            body=json.dumps({'docs': targets})
        )

        indexer_response = ElasticsearchWrapperResponse(response)

        if indexer_response.success:
            if cache_key:
                self._set_cache(cache_key, response, self.CACHE_TTL_SECONDS_MULTI)
        else:
            logging.warning('ORRTA API Error multi searching - {0}'.format(url))

            error_dimensions = {
                'search': dimensions,
                'status_code': indexer_response.status_code,
                'error': indexer_response.response
            }

            measure.count('mget-error', dimensions=error_dimensions)

        return indexer_response

    async def msearch(self, index, doc_type, queries, cache_key=None):
        url = self.generate_es_url(index, doc_type, action='_msearch')

        dimensions = {
            'index': index,
            'doc_type': doc_type,
            'queries': queries,
            'cache_key': cache_key
        }

        measure.count('msearch', dimensions=dimensions)

        headers = self._headers
        headers['Content-Type'] = 'application/json'

        str_queries = [json.dumps(query) for query in queries]

        body = ''

        for str_query in str_queries:
            body += '{index}\n{query}\n'.format(query=str_query, index=json.dumps({"index": index}))

        if cache_key:
            cache_value = self._get_cache(cache_key)

            if cache_value:
                indexer_response = ElasticsearchWrapperResponse(cache_value)
                return indexer_response

        response = await http_client.fetch(
            url,
            method='POST',
            raise_error=False,
            auth_username=settings.ELASTICSEARCH_USER,
            auth_password=settings.ELASTICSEARCH_PASS,
            headers=headers,
            body=body
        )

        indexer_response = ElasticsearchWrapperResponse(response)

        if indexer_response.success:
            if cache_key:
                self._set_cache(cache_key, response, self.CACHE_TTL_SECONDS_MULTI)
        else:
            logging.warning('ORRTA API Error multi searching - {0}'.format(url))

            error_dimensions = {
                'search': dimensions,
                'status_code': indexer_response.status_code,
                'error': get_response_body(indexer_response.response)
            }

            measure.count('msearch-error', dimensions=error_dimensions)

        return indexer_response

    async def index(self, index, doc_type, document, doc_id=None):
        url = self.generate_es_url(index, doc_type, doc_id)

        dimensions = {
            'index': index,
            'doc_type': doc_type,
            'doc_id': doc_id
        }

        measure.count('index', dimensions=dimensions)

        await self.ensure_index_exists(index)

        indexer_response = ElasticsearchWrapperResponse(
            await http_client.fetch(
                url,
                method='POST',
                raise_error=False,
                auth_username=settings.ELASTICSEARCH_USER,
                auth_password=settings.ELASTICSEARCH_PASS,
                headers=self._headers,
                body=json.dumps(document)
            )
        )

        if not indexer_response.success:
            logging.warning('ORRTA API Error inserting document - {0}'.format(url))

            error_dimensions = {
                'search': dimensions,
                'status_code': indexer_response.status_code,
                'error': get_response_body(indexer_response.response)
            }

            measure.count('index-error', dimensions=error_dimensions)

        return indexer_response

    async def update(self, index, doc_type, doc_id, document):
        url = self.generate_es_url(index, doc_type, doc_id, action='_update')
        body = document

        dimensions = {
            'index': index,
            'doc_type': doc_type,
            'doc_id': doc_id
        }

        try:
            measure.count('update', dimensions=dimensions)

            indexer_response = ElasticsearchWrapperResponse(
                await http_client.fetch(
                    url,
                    auth_username=settings.ELASTICSEARCH_USER,
                    raise_error=False,
                    auth_password=settings.ELASTICSEARCH_PASS,
                    method='POST',
                    headers=self._headers,
                    body=json.dumps(body)
                )
            )

            return indexer_response
        except HTTPError as http_error:
            logging.warning('ORRTA API Error updating {0}'.format(url))

            error_dimensions = {
                'search': dimensions,
                'status_code': http_error.code,
                'error': get_response_body(http_error.response)
            }

            measure.count('update-error', dimensions=error_dimensions)

            if http_error.code == 404:
                return ElasticsearchWrapperResponse(http_error.response)

            raise http_error

    async def index_or_update(self, index, doc_type, doc_id, document):
        url = self.generate_es_url(index, doc_type, doc_id)

        try:
            await http_client.fetch(
                url,
                auth_username=settings.ELASTICSEARCH_USER,
                raise_error=False,
                auth_password=settings.ELASTICSEARCH_PASS,
                method='HEAD'
            )

            return await self.update(
                index=index,
                doc_type=doc_type,
                doc_id=doc_id,
                document=document
            )
        except HTTPError:
            return await self.index(
                index=index,
                doc_type=doc_type,
                doc_id=doc_id,
                document=document
            )

    async def remove_field(self, index, doc_type, doc_id, field):
        url = self.generate_es_url(index, doc_type, doc_id, action='_update')

        body = {
            'script': 'ctx._source.remove("{0}")'.format(field)
        }

        try:
            indexer_response = ElasticsearchWrapperResponse(
                await http_client.fetch(
                    url,
                    method='POST',
                    raise_error=False,
                    auth_username=settings.ELASTICSEARCH_USER,
                    auth_password=settings.ELASTICSEARCH_PASS,
                    headers=self._headers,
                    body=json.dumps(body)
                )
            )

            return indexer_response
        except HTTPError as http_error:
            logging.warning('ORRTA API Error removing field {0} - {1}'.format(
                    field, url))

            if http_error.code == 404:
                return ElasticsearchWrapperResponse(http_error.response)

            raise http_error

    async def delete(self, index, doc_type, doc_id):
        url = self.generate_es_url(index, doc_type, doc_id)

        dimensions = {
            'index': index,
            'doc_type': doc_type,
            'doc_id': doc_id
        }

        try:
            measure.count('delete', dimensions=dimensions)

            indexer_response = ElasticsearchWrapperResponse(
                await http_client.fetch(
                    url,
                    method='DELETE',
                    raise_error=False,
                    auth_username=settings.ELASTICSEARCH_USER,
                    auth_password=settings.ELASTICSEARCH_PASS
                )
            )

            return indexer_response
        except HTTPError as http_error:
            logging.warning('ORRTA API Error deleting document - {0}'.format(url))

            error_dimensions = {
                'search': dimensions,
                'status_code': http_error.code,
                'error': get_response_body(http_error.response)
            }

            measure.count('delete-error', dimensions=error_dimensions)

            if http_error.code == 404:
                return ElasticsearchWrapperResponse(http_error.response)

            raise http_error

    async def ensure_index_exists(self, index):
        index_url = self.generate_es_url(index)

        try:
            await http_client.fetch(
                index_url,
                auth_username=settings.ELASTICSEARCH_USER,
                auth_password=settings.ELASTICSEARCH_PASS,
                method='HEAD'
            )
        except HTTPError:
            await self.create_aliased_index(index)

    async def create_aliased_index(self, index):
        real_index_name = ".{index_name}-v1".format(index_name=index)
        alias_name = index

        # creates index
        await http_client.fetch(
            self.generate_es_url(real_index_name),
            auth_username=settings.ELASTICSEARCH_USER,
            raise_error=False,
            auth_password=settings.ELASTICSEARCH_PASS,
            method='PUT',
            headers=self._headers,
            body=json.dumps({})
        )

        # creates alias
        await http_client.fetch(
            self.generate_es_url('_aliases'),
            method='POST',
            raise_error=False,
            auth_username=settings.ELASTICSEARCH_USER,
            auth_password=settings.ELASTICSEARCH_PASS,
            headers=self._headers,
            body=json.dumps({
                'actions': [
                    {
                        'add': {
                            'index': real_index_name,
                            'alias': alias_name
                        }
                    }
                ]
            })
        )

    async def find_doc_by_id(self, index, doc_id):
        url = self.generate_es_url(index, action='_search')
        query = {
            'query': {
                'bool': {'filter': {'term': {'_id': doc_id}}}
            }
        }

        try:
            response = await http_client.fetch(
                url,
                auth_username=settings.ELASTICSEARCH_USER,
                raise_error=False,
                auth_password=settings.ELASTICSEARCH_PASS,
                body=json.dumps(query),
                method='POST',
                headers=self._headers
            )

            json_response = json.loads(response.body.decode('UTF-8'))
            hits = json_response.get('hits', {}).get('hits', [])

            if not hits:
                raise DocumentNotFoundException(doc_id)

            return hits[0]
        except HTTPError:
            raise DocumentNotFoundException(doc_id)

    def generate_es_url(self, index, doc_type=None, doc_id=None, action=None):
        url = '{0}/{1}'.format(settings.ELASTICSEARCH_URL, index)

        if doc_type:
            url += '/{0}'.format(doc_type)

        if doc_id:
            url += '/{0}'.format(urllib.parse.quote_plus(doc_id))

        if action:
            url += '/{0}'.format(action)

        return url

    def _get_cache(self, key):
        try:
            cache_value = redis_connection.get(key)

            if cache_value:
                measure.count('cache-hit', dimensions={
                    'key': key
                })
                return CacheResponse(json.loads(cache_value))

            measure.count('cache-miss', dimensions={
                'key': key
            })
            return None
        except Exception:
            logger.exception('failed to get cache')

    def _set_cache(self, key, response, ttl):
        try:
            cache_value = json.dumps({
                'code': response.code,
                'body': response.body.decode('UTF-8')
            })
            redis_connection.set(key, cache_value, self._get_randon_cache_ttl(ttl))
        except Exception:
            logger.exception('failed to set cache')

    def _get_randon_cache_ttl(self, ttl):
        try:
            return ttl + (600 * randint(1, 30))
        except Exception:
            logger.exception('failed to generate random cache')
            return ttl
