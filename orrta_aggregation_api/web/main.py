import logging
import os

import newrelic.agent
from orrta_aggregation_api import __version__ as version
from orrta_aggregation_api import settings
from orrta_aggregation_api.web.elasticsearch import get_routes as elasticsearch_routes
from orrta_aggregation_api.web.search import get_routes as search_routes
from tornado.web import Application as TornadoApplication
from tornado.web import RequestHandler, URLSpec

logger = logging.getLogger('web')

if os.environ.get('NEW_RELIC_ENABLED', False) == 'True':
    newrelic.agent.initialize('newrelic.ini')


class MainHandler(RequestHandler):
    def get(self):
        self.write("orrta_aggregation_api - {version}".format(version=version))


class Application(TornadoApplication):

    def __init__(self, debug=False):
        logger.info('Initializing orrta_aggregation_api')
        super().__init__(self._routes(), debug=debug)

    def _routes(self):
        routes = (
            [
                URLSpec(r'/?', MainHandler),
                URLSpec(r'/favicon.ico?', MainHandler)
            ] +
            search_routes() +
            elasticsearch_routes()
        )
        return routes


application = Application(debug=settings.DEBUG)
