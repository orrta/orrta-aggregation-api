from measures import Measure
from orrta_aggregation_api import settings
from orrta_aggregation_api.elasticsearch.es_connection import ElasticSearchConnection
from orrta_aggregation_api.web import simple_auth
from tornado.web import RequestHandler, URLSpec
import json
import logging

logger = logging.getLogger('web')

measure = Measure('search-api', (settings.MEASURES_URL, settings.MEASURES_PORT))


def get_routes():
    return [
        URLSpec(r'/search?', SearchHandler),
    ]


@simple_auth
class SearchHandler(RequestHandler):

    @property
    def _es_connection(self):
        return ElasticSearchConnection()

    @property
    def _published_filter(self):
        if settings.ENVIROMENT == 'PROD':
            return [{'term': {'published': {'value': False}}}]

        return []

    @property
    def _search_context(self):
        return {
            'ranking-default': {
                'query': {
                    'size': self.get_argument('size', 6),
                    'from': self.get_argument('from', 0),
                    'query': {'term': {'post_id.keyword': self.get_argument('post_id', None)}},
                    'aggs': {'sum': {'sum': {'field': 'count'}}},
                    'sort': [{'count': {'order': 'desc'}}],
                },
                'cache_key': 'ranking-default--{post_id}--{_from}--{size}'.format(
                    post_id=self.get_argument('post_id', None),
                    _from=self.get_argument('from', 0),
                    size=self.get_argument('size', 6)
                )
            },
            'comments-default': {
                'query': {
                    'size': self.get_argument('size', 6),
                    'from': self.get_argument('from', 0),
                    'sort': [{'date_time': {'order': 'desc'}}],
                    'query': {'term': {'postId.keyword': self.get_argument('post_id', None)}},
                },
                'cache_key': 'comments-default--{post_id}--{_from}--{size}'.format(
                    post_id=self.get_argument('post_id', None),
                    _from=self.get_argument('from', 0),
                    size=self.get_argument('size', 6)
                )
            },

            'timeline-default': {
                'query': {
                    'size': self.get_argument('size', 6),
                    'from': self.get_argument('from', 0),
                    'sort': [{'dateTime': {'order': 'desc'}}],
                    'query': {'bool': {'must_not': self._published_filter}}
                },
                'cache_key': 'timeline-default--{index}--{_from}--{size}'.format(
                    index=self.get_argument('index'),
                    _from=self.get_argument('from', 0),
                    size=self.get_argument('size', 6)
                )
            },

            'timeline-most-loved': {
                'query': {
                    'size': self.get_argument('size', 6),
                    'from': self.get_argument('from', 0),
                    'sort': [{'hearts_count': {'order': 'desc'}}],
                    'query': {'bool': {'must_not': self._published_filter}}
                },
                'cache_key': 'timeline-most-loved--{index}--{_from}--{size}'.format(
                    index=self.get_argument('index'),
                    _from=self.get_argument('from', 0),
                    size=self.get_argument('size', 6)
                )
            },

            'timeline-search': {
                'query': {
                    'size': self.get_argument('size', 10),
                    'from': self.get_argument('from', 0),
                    'query': {
                        'bool': {
                            'must_not': self._published_filter,
                            'must': [{
                                'multi_match': {
                                    'fields': ['title^3 ', 'ownerComment^2', 'content^1'],
                                    'query': self.get_argument('search_term', None),
                                    'fuzziness': 'AUTO'
                                }
                            }]
                        }
                    }
                },
                'cache_key': 'timeline-search--{index}--{search_term}--{_from}--{size}'.format(
                    index=self.get_argument('index'),
                    search_term=self.get_argument('search_term', None),
                    _from=self.get_argument('from', 0),
                    size=self.get_argument('size', 10)
                )
            }

        }[self.get_argument('query_name')]

    async def get(self):
        logger.info('Searching')

        index = self.get_argument('index')

        query = self._search_context['query']

        if int(query.get('from', 0)) > 200 or int(query.get('size', 0)) > 40:
            measure.count('search-bad-request', dimensions={
                'index': index,
                'query': query,
                'cache_key': self._search_context['cache_key']
            })
            self.set_status(400)
            self.write('Bad Request')
            return

        indexer_response = await self._es_connection.search(
            index=index,
            query=query,
            cache_key=self._search_context['cache_key']
        )

        if indexer_response.is_valid:

            indexer_response = await self._run_includes(indexer_response)

            self.set_status(indexer_response.es_wrapper_response.status_code)
            self.write(json.dumps({
                'es': indexer_response.es_wrapper_response.response_json
            }))

            measure.count('search-success', dimensions={
                'index': index,
                'query': self._search_context['query'],
                'cache_key': self._search_context['cache_key']
            })
        else:
            self.set_status(400)
            self.write(json.dumps({
                'errors': indexer_response.validation_errors
            }))

            measure.count('search-error', dimensions={
                'index': index,
                'query': self._search_context['query'],
                'cache_key': self._search_context['cache_key'],
                'errors': indexer_response.validation_errors
            })

    async def _run_includes(self, indexer_response):
        include = self.get_argument('include', None)

        if not include:
            return indexer_response

        if include == 'ranking':
            indexer_response = await self._include_ranking(indexer_response)

        if include == 'comment-user-picture':
            indexer_response = await self._include_comment_user_picture(indexer_response)

        measure.count('search-include', dimensions={
            'include': include
        })

        return indexer_response

    async def _include_ranking(self, indexer_response):
        logger.info('Loading ranking')
        try:
            msearch_queries = [{
                'query': {'term': {'post_id.keyword': hit['_id']}},
                'sort': [{'count': {'order': 'desc'}}],
                'size': 5
            } for hit in indexer_response.es_wrapper_response.response_json['hits']['hits']]

            msearch_response = await self._es_connection.msearch(
                index='hearts-sending',
                queries=msearch_queries,
                cache_key='{key}--{include_key}'.format(
                    key=self._search_context['cache_key'],
                    include_key='include-hearts-sending'
                )
            )

            rankings = [ranking['hits']['hits'] for ranking in msearch_response.es_wrapper_response.response_json['responses']]

            for index, post in enumerate(indexer_response.es_wrapper_response.response_json['hits']['hits']):
                post['_source']['ranking'] = [r['_source'] for r in rankings[index]]
        except Exception:
            for index, post in enumerate(indexer_response.es_wrapper_response.response_json['hits']['hits']):
                post['_source']['ranking'] = []
            logger.exception('Error to load ranking')
        finally:
            return indexer_response

    async def _include_comment_user_picture(self, indexer_response):
        logger.info('Loading ranking')
        try:
            mget_targets = [{
                '_id': hit['_source']['userId'],
                '_type': 'default',
                '_source': ['photo', 'authType']
            } for hit in indexer_response.es_wrapper_response.response_json['hits']['hits']]

            mget_response = await self._es_connection.mget(
                index='user',
                targets=mget_targets,
                cache_key='{key}--{include_key}'.format(
                    key=self._search_context['cache_key'],
                    include_key='include-comments-user-picture'
                )
            )

            mget_response_json = mget_response.es_wrapper_response.response_json

            if mget_response_json:
                users = [
                    doc['_source']
                    for doc
                    in mget_response_json['docs']
                ]

                for index, comment in enumerate(indexer_response.es_wrapper_response.response_json['hits']['hits']):
                    photo = users[index].get('photo')

                    if photo and users[index].get('authType') == 'google':
                        comment['_source']['imageUrl'] = photo
        except Exception:
            logger.exception('Error to load users images url.')
        finally:
            return indexer_response
