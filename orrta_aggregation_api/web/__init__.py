from orrta_aggregation_api import settings


def simple_auth(handler_class):

    def wrap_execute(handler_execute):
        def authorize_access(handler, kwargs):
            auth_header = handler.request.headers.get('Authorization', False)

            if not auth_header:
                auth_header = handler.request.headers.get('X-App-Authorization', False)

            if not auth_header or auth_header != settings.AUTH_KEY:
                handler.set_status(401)
                handler._transforms = []
                handler.finish()
                return False

            return True

        def _execute(self, transforms, *args, **kwargs):
            if not authorize_access(self, kwargs):
                return False

            return handler_execute(self, transforms, *args, **kwargs)

        return _execute

    handler_class._execute = wrap_execute(handler_class._execute)
    return handler_class
