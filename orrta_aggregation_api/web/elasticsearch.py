import json
import logging

from tornado.web import HTTPError, RequestHandler, URLSpec
from orrta_aggregation_api.elasticsearch.es_connection import ElasticSearchConnection
from orrta_aggregation_api.web import simple_auth
from orrta_aggregation_api import settings

logger = logging.getLogger('web')


def get_routes():
    return [
        URLSpec(r'/([^/]+)/?', ElasticsearchHandler),
        URLSpec(r'/([^/]+)/_search?', SearchHandler),
        URLSpec(r'/([^/]+)/([^/]+)/?', ElasticsearchHandler),
        URLSpec(r'/([^/]+)/([^/]+)/update/?', UpdateHandler)
    ]


@simple_auth
class BaseHandler(RequestHandler):
    @property
    def es_connection(self):
        return ElasticSearchConnection()

    def request_json(self):
        try:
            return json.loads(self.request.body.decode('utf-8'))
        except json.decoder.JSONDecodeError:
            raise HTTPError(400, 'Bad Request')

    def write_error(self, status_code, **kwargs):
        if status_code == 500:

            self.write(json.dumps({
                'errors': [
                    'Internal server error'
                ]
            }))
        else:
            logger.info('Unhadled status_code in write_error: {status_code}'.format(status_code=status_code))


class SearchHandler(BaseHandler):

    async def get(self, index):
        await self._search(index)

    async def post(self, index):
        await self._search(index)

    async def _search(self, index):
        logger.info('Searching - index: {index}'.format(index=index))
        indexer_response = await self.es_connection.search(index=index, query=self._get_request_json())

        if indexer_response.is_valid:
            self.set_status(indexer_response.es_wrapper_response.status_code)
            self.write(json.dumps({
                'es': indexer_response.es_wrapper_response.response_json
            }))
        else:
            self.set_status(400)
            self.write(json.dumps({
                'errors': indexer_response.validation_errors
            }))

    def _get_request_json(self):
        try:
            return self.request_json()
        except Exception:
            return {}


class ElasticsearchHandler(BaseHandler):

    async def get(self, index, doc_id):
        logger.info('Getting - index: {index}, doc_id: {doc_id}'.format(index=index, doc_id=doc_id))

        if index == 'user' and settings.CONTROLLED_ERROR:
            self.set_status(500)
            self.write(json.dumps({'controlled': True}))
            return

        indexer_response = await self.es_connection.get(index=index, doc_id=doc_id)

        if indexer_response.is_valid:
            self.set_status(indexer_response.es_wrapper_response.status_code)

            result = {
                'es': indexer_response.es_wrapper_response.response_json
            }

            if index == 'user':
                result['settings'] = settings.MOBILE_APP_SETTINGS

            self.write(json.dumps(result))
        else:
            self.set_status(400)
            self.write(json.dumps({
                'errors': indexer_response.validation_errors
            }))

    async def post(self, index, doc_id=None):
        await self._index_document(index, doc_id)

    async def put(self, index, doc_id=None):
        await self._index_document(index, doc_id)

    async def delete(self, index, doc_id):
        await self._delete_document(index, doc_id)

    async def _index_document(self, index, doc_id):
        logger.info('Indexing - index: {index}, doc_id: {doc_id}'.format(index=index, doc_id=doc_id))

        indexer_response = await self.es_connection.index(
            index=index,
            doc_id=doc_id,
            document=self.request_json()
        )

        if indexer_response.is_valid:
            self.set_status(indexer_response.es_wrapper_response.status_code)
            self.write(json.dumps({
                'es': indexer_response.es_wrapper_response.response_json
            }))
        else:
            self.set_status(400)
            self.write(json.dumps({
                'errors': indexer_response.validation_errors
            }))

    async def _delete_document(self, index, doc_id):
        logger.info('Deleting - index: {index}, doc_id: {doc_id}'.format(index=index, doc_id=doc_id))

        indexer_response = await self.es_connection.delete(index=index, doc_id=doc_id)

        self.set_status(indexer_response.es_wrapper_response.status_code)

        self.write(json.dumps({
            'es': indexer_response.es_wrapper_response.response_json
        }))


class UpdateHandler(BaseHandler):
    async def post(self, index, doc_id):
        await self._update(index, doc_id)

    async def patch(self, index, doc_id):
        await self._update(index, doc_id)

    async def _update(self, index, doc_id):
        logger.info('Updating - index: {index}, doc_id: {doc_id}'.format(index=index, doc_id=doc_id))

        indexer_response = await self.es_connection.update(
            index=index,
            doc_id=doc_id,
            document=self.request_json()
        )

        self.set_status(indexer_response.es_wrapper_response.status_code)

        self.write(json.dumps({
            'es': indexer_response.es_wrapper_response.response_json
        }))
