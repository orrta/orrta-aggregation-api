help:
	@echo 'Makefile for orrta_aggregation_api                                            '
	@echo '                                                                             '
	@echo 'Usage:                                                                       '
	@echo '   install                                 Install all dependencies to run   '
	@echo '   setup                                   Install all dependencies to dev   '
	@echo '   run_local                               Run project using local enviroment'
	@echo '   run_elasticsearch                       Run elasticisearch using docker   '
	@echo '   stop_elasticsearch                      Stop elasticsearch container      '
	@echo '   run_kibana                              Run Kibana container              '
	@echo '   test                                    Run project tests                 '
	@echo '   test_unit                               Run project unit tests            '
	@echo '   test_acceptance                         Run project acceptance tests      '

install:
	@pip install -r requirements.txt

setup:
	@pip install -r requirements_dev.txt

run_local:
	PYTHONUNBUFFERED=1 \
	ELASTICSEARCH_PASS="u5Gcy1Q12saAZqP8A1tcBO1k" \
	ELASTICSEARCH_URL="https://5618aac2b24848f8877c7a43ee1340c9.us-east-1.aws.found.io:9243" \
	ELASTICSEARCH_USER="elastic" \
	NEW_RELIC_ENABLED="False" \
	PORT=8284 \
	python web.py

test: test_code_quality test_unit

test_code_quality:
	@echo "========================== TEST CODE QUALITY =========================="
	@py.test orrta_aggregation_api --pep8 --flakes --mccabe

test_unit:
	@echo "============================= UNIT TESTS ============================="
	@py.test tests/unit --cov-report term-missing --cov-report xml --cov=orrta_aggregation_api --spec

test_acceptance: run_elasticsearch
	@echo "========================= ACCEPTANCE TESTS ==========================="
	@echo "\nWaiting 30 seconds while Elasticsearch is starting..."

	@sleep 30

	NEW_RELIC_ENABLED="False" \
	REDIS_URL="" \
	py.test tests/acceptance --spec

	make stop_elasticsearch

run_elasticsearch:
	@docker-compose -f tests/acceptance/env/docker-compose.yml up -d

stop_elasticsearch:
	@docker-compose -f tests/acceptance/env/docker-compose.yml down

open_kibana:
	open "http://localhost:5601"

clean:
	@find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
